#-------------------------------------------------
#
# Project created by QtCreator 2012-01-26T11:27:46
#
#-------------------------------------------------

QT       += core gui

TARGET = EasyTheater
TEMPLATE = app


SOURCES += main.cpp\
        easytheater.cpp \
    io.cpp \
    database.cpp \
    fade.cpp

HEADERS  += easytheater.h \
    io.h \
    TypeDefs.h \
    database.h \
    fade.h

FORMS    += easytheater.ui
