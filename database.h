#ifndef DATABASE_H
#define DATABASE_H

#include <string>
#include <vector>

class Database
{
    //Current DMX Value Matrix
    unsigned char currentValues[UNIVERSES][512];
    unsigned int currentCue;

    //Cue Structure
   struct cue {
       string name;
       string text;
       unsigned int time;
       unsigned char values[UNIVERSES][512];

       };
    //Vector of Cues -> Generating CueList

   vector<cue> cuelist ();
   //Live-Output-Mode
   bool live;


public:
    Database();
    string getCueName(unsigned int CueID);
    string getCueText(unsigned int CueID);
    unsigned int getCueTime(unsigned int CueID);
    unsigned char getCueValues(unsigned int CueID, unsigned char universe, unsigned short channel);
    unsigned int getCurrentCue();
    void setCurrentCue(unsigned int CueID);
    bool getMode();



};

#endif // DATABASE_H
