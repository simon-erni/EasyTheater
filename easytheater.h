#ifndef EASYTHEATER_H
#define EASYTHEATER_H

#include <QMainWindow>

namespace Ui {
class EasyTheater;
}

class EasyTheater : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit EasyTheater(QWidget *parent = 0);
    ~EasyTheater();
    
private slots:
    void on_actionNeu_triggered();

    void on_actionLaden_triggered();

private:
    Ui::EasyTheater *ui;
};

#endif // EASYTHEATER_H
