#include "easytheater.h"
#include "ui_easytheater.h"
#include <QFileDialog>
#include <QString>
#include "database.h"

EasyTheater::EasyTheater(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::EasyTheater)
{
    ui->setupUi(this);
}

EasyTheater::~EasyTheater()
{
    delete ui;
}

void EasyTheater::on_actionNeu_triggered()
{


}

void EasyTheater::on_actionLaden_triggered()
{
    //Opens a FileDialog for *.et - Files and stores the Path in the fileName String.

    QFileDialog dialog(this);
    dialog.setNameFilter(tr("EasyTheater Files (*.et)"));
    dialog.setViewMode(QFileDialog::Detail);
    dialog.setFileMode(QFileDialog::ExistingFile);
    QString fileName;
    if (dialog.exec())fileName = dialog.getOpenFileName();
    //Now execute the LoadFunction

}
